import java.util.ArrayList;


public class ArrayListKM {

    public static void main(String[] args) {

        ArrayList<String> people = new ArrayList<String>();
        // добавим в список ряд элементов c .add
        people.add("Алиса");
        people.add("Андрей");
        people.add("Рустам");
        people.add("Джек");
        people.add("Даниэль");
        people.add("Олег");
        people.add(1, "Яков"); // добавляем элемент по индексу 1


        // Узнаю кол-во элементов в методе size()
        System.out.printf("Кол-во клиентских менеджеров  %d  \n", people.size());
        for (String person : people) {

            System.out.println("КМ " + person);
        }
        // проверяю наличие элемента
        if (people.contains("Олег")) {


            System.out.println("Список клиентских менеджеров " + people);
        }


    }
}


